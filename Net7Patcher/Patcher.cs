// Copyright (c) Meelis Mägi. Licensed under MIT license. See LICENSE in the project root for license information

using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DamienG.Security.Cryptography;

namespace Net7
{
    class Patcher
    {
        private const string UserAgent = "Net7Patcher";

        private string name;
        private Uri patchUri;
        private string path;

        private List<string> excludeFiles;

        private Hashtable localFilesMap;
        private Hashtable localDirectoryMap;

        private VersionFile localVersionFile;
        private ChecksumFilesList localChecksumFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="Net7.Patcher"/> class.
        /// </summary>
        /// <param name="uri">URI.</param>
        /// <param name="name">Name.</param>
        /// <param name="path">Path.</param>
        public Patcher(string uri, string name, string path)
        {
            this.patchUri = new Uri(new Uri(uri), name + "/");
            this.name = name;
            this.path = path.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar.ToString();

            localVersionFile = new VersionFile(Path.Combine(path, name + "-Version.txt"));
            localChecksumFile = new ChecksumFilesList(Path.Combine(path, name + "-FileList.txt"));

            excludeFiles = new List<string>();
        }

        public void addToExclude(string[] patterns)
        {
            foreach (string p in patterns)
            {
                if (p.Length > 0)
                {
                    excludeFiles.Add(p);
                }
            }
            // sort by length (longer first)
            excludeFiles.Sort((string x, string y) => (x.Length > y.Length) ? -1 : 0);
        }

        public void VerifyAndDownload(bool forceFullScan)
        {
            Progress("Scanning local files");
            localFilesMap = new Hashtable();
            localDirectoryMap = new Hashtable();
            ScanDirectory(path);

            if (!forceFullScan && IsVersionCurrent())
            {
                Progress("Version is current");
                VerifyAndDownloadWorker(true);
            }
            else
            {
                if (VerifyAndDownloadWorker(false))
                {
                    localVersionFile.save();
                }
            }
        }

        protected bool VerifyAndDownloadWorker(bool quick)
        {
            if (quick)
            {
                Progress("Quick verifying files...");
            }
            else
            {
                Progress("Verifying files...");
            }

            Uri url = new Uri(patchUri, "Files.txt");
            string data = DownloadString(url.ToString());

            localChecksumFile.ParseString(data.Split(new char[] { '\n', '\r' }));

            float progress = 0;
            float step = (float)100 / localChecksumFile.files.Count;
            int missingBytes = 0;
            Dictionary<string, ChecksumFile> missing = new Dictionary<string, ChecksumFile>();
            foreach (ChecksumFile file in localChecksumFile.files)
            {
                if (IsIgnored(file.filename))
                {
                    continue;
                }

                Progress(String.Format("({1:F1}%) {0}", file.filename, progress), false);

                string local = FindLocalFile(file.filename);
                bool check = !quick || ForceCheck(file.filename);
                if (check && IsFileChanged(local, file.size, file.crc))
                {
                    missing.Add(local, file);
                    missingBytes += file.size;
                }
                progress += step;
            }


            if (missing.Count > 0)
            {
                int missingCount = missing.Count;
                foreach (KeyValuePair<string, ChecksumFile> pair in missing)
                {
                    Progress(String.Format("Downloading {0} files ({1:N0} bytes)...", missingCount, missingBytes));
                    if (!DownloadFile(pair.Value, pair.Key))
                    {
                        return false;
                    }
                    missingBytes -= pair.Value.size;
                    missingCount--;

                    // FIXME: maybe create function to MainClass for this
                    //FIXME: Net4.5 if (!Console.IsOutputRedirected)
                    {
                        // move back to previous line
                        Console.CursorTop--;
                    }
                }
            }
            else
            {
                Progress("All files are current");
            }

            return true;
        }

        protected bool DownloadFile(ChecksumFile file, string local)
        {
            try
            {
                // FIXME: catch error when path is read-only

                string tmp = local + ".tmp";
                Uri uri = new Uri(patchUri, "Files/" + file.filename);
                Progress(String.Format("> {0} --> {1}", file.filename, tmp), false);

                string tmppath = Path.GetDirectoryName(tmp);
                if (!Directory.Exists(tmppath))
                {
                    Directory.CreateDirectory(tmppath);
                }

                DownloadThread worker = new DownloadThread(uri, tmp);

                Thread thread = new Thread(new ThreadStart(worker.ThreadRun));

                Progress("Start downloading (" + uri.ToString() + ")", false);
                thread.Start();
                while (thread.IsAlive)
                {
                    Progress(String.Format("({0:N0}%) {1}", worker.ProgressPercent, uri.ToString()), false);
                    System.Threading.Thread.Sleep(100);
                }
                if (worker.Failed)
                {
                    Progress("ERROR: " + worker.Message);
                    return false;
                }

                string hash = getCrc32(tmp);
                if (!file.crc.Equals(hash))
                {
                    File.Delete(tmp);
                    if (worker.Received == 0)
                    {
                        Progress("failed (" + uri.ToString() + ")");
                    }
                    else
                    {
                        Progress(String.Format("ERROR: downloaded file ({0}) checksum failed", local));
                    }
                    return false;
                }
                if (File.Exists(local))
                {
                    File.Delete(local);
                }
                File.Move(tmp, local);

            }
            catch (ThreadStartException e)
            {
                Console.WriteLine();
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        protected bool IsFileChanged(string file, int size, string hash)
        {
            bool isChanged = false;
            if (File.Exists(file))
            {
                FileInfo fi = new FileInfo(file);
                if (fi.Length == size)
                {
                    string newhash = getCrc32(file);
                    if (!hash.Equals(newhash))
                    {
                        Progress(String.Format("hash mismatch (our:{0}, their:{1})", newhash, hash));
                        isChanged = true;
                    }
                }
                else
                {
                    Progress(String.Format("size mismatch (our:{0:N0}, their:{1:N0})", fi.Length, size));
                    isChanged = true;
                }
            }
            else
            {
                Progress(String.Format("missing ({0})", file));
                isChanged = true;
            }

            return isChanged;
        }

        protected bool ForceCheck(string file)
        {
            string ext = Path.GetExtension(file).ToLower();
            switch (ext)
            {
                case ".exe":
                case ".dll":
                case ".ini":
                case ".dat":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Map file into local file system in case-insenstitive way
        /// </summary>
        /// <returns>The local file.</returns>
        /// <param name="file">File.</param>
        protected string FindLocalFile(string file)
        {
            string ret = "";
            string key = file.ToLowerInvariant();
            if (localFilesMap.Contains(key))
            {
                ret = (string)localFilesMap[key];
            }
            if (ret.Length == 0)
            {
                string dir = FindLocalDirectory(file).TrimEnd(Path.DirectorySeparatorChar);
                if (dir.Length > 0)
                {
                    // replace file path with first known local directory (leaving rest Camel/Case.ext)
                    dir += Path.DirectorySeparatorChar.ToString();
                    file = Path.Combine(dir, file.Substring(dir.Length));
                }
                ret = Path.Combine(path, file);
            }
            return ret;
        }

        /// <summary>
        /// Map dir into local filesystem in case-insensitive way
        /// </summary>
        /// <returns>The local directory.</returns>
        /// <param name="dir">Dir.</param>
        protected string FindLocalDirectory(string dir)
        {
            if (String.IsNullOrEmpty(dir))
            {
                return "";
            }
            dir = dir.ToLowerInvariant();
            if (localDirectoryMap.Contains(dir))
            {
                return (string)localDirectoryMap[dir];
            }
            // move up to parent
            return FindLocalDirectory(Path.GetDirectoryName(dir));
        }

        /// <summary>
        /// Determines whether local files version is same as remote version
        /// </summary>
        /// <returns><c>true</c> if version is current; otherwise, <c>false</c>.</returns>
        protected bool IsVersionCurrent()
        {
            Uri url = new Uri(patchUri, "Version.txt");

            int remoteVersion = Convert.ToInt32(DownloadString(url.ToString()));
            if (remoteVersion == 0)
            {
                throw new WebException(String.Format("Failed to download Version.txt from '{0}'", url.ToString()));
            }

            localVersionFile.RemoteVersion = remoteVersion;
            Progress(String.Format("local version {0}, remote version {1}", localVersionFile.LocalVersion, localVersionFile.RemoteVersion));

            return localVersionFile.IsCurrent;
        }

        /// <summary>
        /// Recursively scan local directory and build lowercase:filename map
        /// </summary>
        /// <param name="dir">directory</param>
        protected void ScanDirectory(string dir)
        {
            int index = path.Length;
            string reldir = dir.Substring(index).ToLowerInvariant();
            if (reldir.Length > 0)
            {
                localDirectoryMap.Add(reldir, dir.Substring(index));
            }

            try
            {
                foreach (string f in Directory.GetFiles(dir))
                {
                    string key = f.Substring(index).ToLowerInvariant();
                    localFilesMap.Add(key, f);
                }

                foreach (string d in Directory.GetDirectories(dir))
                {
                    this.ScanDirectory(d);
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Check if file should be ignored (ends with known pattern)
        /// </summary>
        /// <returns><c>true</c> if there was match, <c>false</c> otherwise</returns>
        /// <param name="file">file name</param>
        protected bool IsIgnored(string file)
        {
            file = file.ToLowerInvariant();
            return !String.IsNullOrEmpty(excludeFiles.Find((string s) => file.EndsWith(s)));
        }

        /// <summary>
        /// Download content from url and return it as string
        /// </summary>
        /// <returns>string</returns>
        /// <param name="url">URL</param>
        protected string DownloadString(string url)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("user-agent", UserAgent);

                    return client.DownloadString(url);
                }
            }
            catch (WebException ex)
            {
                throw new WebException(String.Format("WebClient exception while downloading file ({0}): {1}", url, ex.Message));
            }
        }

        protected string getCrc32(string file)
        {
            Crc32 crc32 = new Crc32();
            string ret = "";
            using (FileStream fs = File.Open(file, FileMode.Open))
            {
                foreach (byte b in crc32.ComputeHash(fs))
                {
                    ret += b.ToString("x2").ToUpper();
                }
            }
            return ret;
        }

        private void Progress(string line, bool newline = true)
        {
            MainClass.Progress(name + ": " + line, newline);
        }
    }
}

