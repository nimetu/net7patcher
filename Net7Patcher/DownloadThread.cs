// Copyright (c) Meelis Mägi. Licensed under MIT license. See LICENSE in the project root for license information

using System;
using System.Net;
using System.Threading;

namespace Net7
{
    public class DownloadThread
    {
        private Uri uri;
        private string local;

        public long Received
        {
            get { return bytesReceived; }
        }

        private long bytesReceived;

        public long Total
        {
            get { return totalBytes; }
        }

        private long totalBytes;

        public int ProgressPercent
        {
            get { return progressPercent; }
        }

        private int progressPercent;

        public bool Failed;
        public string Message;

        public DownloadThread(Uri uri, string local)
        {
            this.uri = uri;
            this.local = local;

            bytesReceived = 0;
            totalBytes = 0;
            progressPercent = 0;

            Failed = false;
            Message = "";
        }

        public void ThreadRun()
        {
            using (WebClient client = new WebClient())
            {
                var notifier = new AutoResetEvent(false);

                client.DownloadFileCompleted += delegate(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
                {
                    if (e.Error != null)
                    {
                        Failed = true;
                        Message = e.Error.Message;
                    }
                    notifier.Set();
                };

                client.DownloadProgressChanged += delegate(object sender, DownloadProgressChangedEventArgs e)
                {
                    bytesReceived = e.BytesReceived;
                    totalBytes = e.TotalBytesToReceive;
                    progressPercent = e.ProgressPercentage;
                };

                client.DownloadFileAsync(uri, local);

                notifier.WaitOne();
            }
        }
    }
}

