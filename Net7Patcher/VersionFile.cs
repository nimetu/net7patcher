// Copyright (c) Meelis Mägi. Licensed under MIT license. See LICENSE in the project root for license information

using System;
using System.IO;

namespace Net7
{
    class VersionFile
    {
        private string path;

        private int localVersion;
        private int remoteVersion;

        public bool IsCurrent
        {
            get { return localVersion == remoteVersion; }
        }

        public int RemoteVersion
        {
            get { return remoteVersion; }
            set { remoteVersion = value; }
        }

        public int LocalVersion
        {
            get { return localVersion; }
        }

        public VersionFile(string path)
        {
            this.path = path;

            if (File.Exists(path))
            {
                localVersion = Convert.ToInt32(File.ReadAllText(path));
            }
            else
            {
                localVersion = 0;
            }
        }

        public void save()
        {
            File.WriteAllText(path, remoteVersion.ToString());

            localVersion = remoteVersion;
        }
    }
}
