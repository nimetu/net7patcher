// Copyright (c) Meelis Mägi. Licensed under MIT license. See LICENSE in the project root for license information

using System;
using System.IO;
using System.Collections.Generic;

namespace Net7
{
    class ChecksumFile
    {
        public string filename;
        public string crc;
        public int size;

        public ChecksumFile(string filename, string crc, int size)
        {
            this.filename = filename;
            this.crc = crc;
            this.size = size;
        }
    }

    class ChecksumFilesList
    {
        private string path;

        public List<ChecksumFile> files;

        public ChecksumFilesList(string path)
        {
            this.path = path;
            files = new List<ChecksumFile>();
        }

        public bool HasLocalFile()
        {
            return File.Exists(path);
        }

        public void ParseLocalFile()
        {
            ParseString(File.ReadAllLines(path));
        }

        public void ParseString(string[] lines)
        {
            files.Clear();

            foreach (string line in lines)
            {
                string[] fragments = line.Split('\t');
                if (fragments.Length < 3)
                {
                    continue;
                }
                string fname = fragments[0].TrimStart(new char[] { '.', '/', '\\' });
                files.Add(new ChecksumFile(fname, fragments[1], Convert.ToInt32(fragments[2])));
            }
        }

        public void save()
        {
            StreamWriter writer = File.CreateText(path);
            foreach (ChecksumFile file in files)
            {
                writer.WriteLine(String.Format("{0}\t{1}\t{2}", file.filename, file.crc, file.size));
            }
            writer.Close();
        }
    }
}
