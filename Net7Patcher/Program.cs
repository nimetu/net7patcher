// Copyright (c) Meelis Mägi. Licensed under MIT license. See LICENSE in the project root for license information

using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

namespace Net7
{
    class MainClass
    {
        public const string Version = "v0.1";

        private const int ERROR_ARGS = 1;
        private const int ERROR_PATCH = 2;

        private static bool quiet = false;
        private static bool textOnly;

        private static string uri = "http://patch.net-7.org/";

        private static string net7path = "";

        private static string clientpath = "";

        private static bool forceFullScan = false;

        private static List<string> excludeFrom;

        private static string[] buildInExcludes = {
                                                  "data/client/mixfiles/eb_sizzle.bik",
                                                  "data/client/mixfiles/eb_ws_logo.bik",
                                                  "data/client/output/keymap.ini",
                                                  "data/client/ini/reg_regdata_org",
                                                  ".log",
                                                  ".th6"
                                              };

        private static Stopwatch stopWatch;

        public static void Main(string[] args)
        {
            if (args.Length == 0 || args[0] == "--help")
            {
                usage();
                Environment.Exit(0);
            }
            textOnly = false;//FIXME: Net4.5 Console.IsOutputRedirected;

            stopWatch = new Stopwatch();
            stopWatch.Start();

            excludeFrom = new List<string>();

            if (!ParseCommandArguments(args))
            {
                Environment.Exit(ERROR_ARGS);
            }

            excludeFrom = excludeFrom.ConvertAll(s => s.ToLowerInvariant());

            // don't catch excptions here, so we get stack trace
            if (net7path.Length > 0)
            {
                Progress("Verifying Net7");
                Patcher patcher = new Patcher(uri, "Net7", net7path);
                patcher.addToExclude(excludeFrom.ToArray());
                patcher.addToExclude(buildInExcludes);

                patcher.VerifyAndDownload(forceFullScan);
            }

            // don't catch excptions here, so we get stack trace
            if (clientpath.Length > 0)
            {
                Progress("Verifying Client");
                Patcher patcher = new Patcher(uri, "Client", clientpath);
                patcher.addToExclude(excludeFrom.ToArray());
                patcher.addToExclude(buildInExcludes);

                // this is in files list, but missing from web server
                patcher.addToExclude(new string[] {
					"rg_regdata_org" 
				});

                patcher.VerifyAndDownload(forceFullScan);
            }

            Console.WriteLine();
        }

        public static void Progress(string line, bool newline = true)
        {
            // FIXME: Console does not like when window size is changed
            if (!quiet)
            {
                if (textOnly)
                {
                    Console.WriteLine(line);
                }
                else
                {
                    float elapsed = (float)stopWatch.ElapsedMilliseconds / 1000;

                    Console.CursorLeft = 0;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("[{0:0.0}s]", elapsed);
                    Console.ResetColor();
                    Console.Write(" ");

                    int spaceAvailable = Console.BufferWidth - Console.CursorLeft;
                    if (line.Length > spaceAvailable)
                    {
                        int keepLeft = (int)(spaceAvailable * 0.4);
                        int keepRight = spaceAvailable - keepLeft - 4;
                        line = line.Substring(0, keepLeft) + "..." + line.Substring(line.Length - keepRight, keepRight);
                    }
                    Console.Write(line);

                    int col = Console.CursorLeft;
                    Console.Write(new String(' ', Console.BufferWidth - Console.CursorLeft - 1));
                    Console.CursorLeft = col;

                    if (newline)
                    {
                        Console.WriteLine();
                    }
                }
            }
        }

        private static void usage()
        {
            Console.WriteLine("Net7Patcher {0} - {1}", Version, RetrieveLinkerTimestamp());
            Console.WriteLine("    --quiet                          - show only errors");
            Console.WriteLine("    --stdout                         - play nice if output is redirected");
            Console.WriteLine("    --url <http://patch.net7.org/>   - (optional) set patch domain");
            Console.WriteLine();
            Console.WriteLine("Select what to patch (one of these must be set)");
            Console.WriteLine("    --net7 <path-to/net7proxy.exe>   - patch net7proxy.exe location");
            Console.WriteLine("    --client <path-to/client.exe>    - patch EnB client.exe location");
            Console.WriteLine("    --force                          - force full scan");
            Console.WriteLine();
            Console.WriteLine("Ignore files from patch (optional)");
            Console.WriteLine("    --exclude <file>                 - ignore files ending with pattern");
            Console.WriteLine("    --exclude-from <file.list>       - load ignore patterns from file");
            Console.WriteLine("    --no-default-excludes            - disable built-in excludes list");
            Console.WriteLine();
            Console.WriteLine("Built-in excludes list:");
            foreach (string s in buildInExcludes)
            {
                Console.WriteLine("    " + s);
            }
        }

        private static bool ParseCommandArguments(string[] args)
        {
            bool success = true;

            int m = args.Length;
            for (int i = 0; i < m; i++)
            {
                switch (args[i])
                {
                    case "--help":
                        usage();
                        return false;
                    case "--quiet":
                        quiet = true;
                        break;
                    case "--stdout":
                        textOnly = true;
                        break;
                    case "--url":
                        uri = args[++i];
                        break;
                    case "--net7":
                        net7path = StripPathComponents(args[++i], new string[] { "net7proxy.exe", "bin" });
                        if (!Directory.Exists(net7path))
                        {
                            System.Console.WriteLine("{0}: net7 path not found '{1}'", GetProgramName(), net7path);
                            success = false;
                        }
                        break;
                    case "--client":
                        clientpath = StripPathComponents(args[++i], new string[] { "client.exe", "release" });
                        if (!Directory.Exists(clientpath))
                        {
                            Console.WriteLine("{0}: client path not found '{1}'", GetProgramName(), clientpath);
                            success = false;
                        }
                        break;
                    case "--force":
                        forceFullScan = true;
                        break;
                    case "--no-default-excludes":
                        buildInExcludes = new string[] {};
                        break;
                    case "--exclude":
                        excludeFrom.Add(args[++i]);
                        break;
                    case "--exclude-from":
                        string fromfile = args[++i];
                        if (File.Exists(fromfile))
                        {
                            excludeFrom.AddRange(File.ReadAllLines(fromfile));
                        }
                        else
                        {
                            Console.WriteLine("{0}: --exclude-from file not found '{1}'", GetProgramName(), fromfile);
                            success = false;
                        }
                        break;
                    default:
                        Console.WriteLine("{0}: unrecognized option '{1}'", GetProgramName(), args[i]);
                        break;
                }
            }

            if (net7path.Length == 0 && clientpath.Length == 0)
            {
                Console.WriteLine("{0}: either --net7 or --client option must be present", GetProgramName());
                success = false;
            }

            return success;
        }

        private static string GetProgramName()
        {
            return Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location);
        }

        private static string StripPathComponents(string path, string[] components)
        {
            path = path.TrimEnd(Path.DirectorySeparatorChar);

            foreach (string s in components)
            {
                if (path.ToLower().EndsWith(s))
                {
                    path = Path.GetDirectoryName(path);
                }
            }

            return path;
        }

        /**
         * http://stackoverflow.com/a/1600990
         * http://discourse.codinghorror.com/t/determining-build-date-the-hard-way/1131/7
         */
        private static DateTime RetrieveLinkerTimestamp()
        {
            string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                }
            }

            int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.ToLocalTime();
            return dt;
        }
    }
}
